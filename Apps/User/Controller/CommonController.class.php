<?php
namespace User\Controller;
use Think\Controller;
class CommonController extends Controller {
    public $ac,$md,$con;
    public $mid,$apid;
    public $user,$ap;

    public function _initialize (){
        $this->ac = ACTION_NAME;
        $this->con = CONTROLLER_NAME;
        $this->md = MODULE_NAME;


        $this->mid = session('uid');

        if (!$this->mid) {
            $this->display('Public/login');
            exit();
        }
        
        $this->assign('md', $this->md);
        $this->assign('ac', $this->ac);
        $this->assign('con', $this->con);
        $this->assign('mid', $this->mid);

        $this->apid = I('apid');
        $this->assign('apid', $this->apid);
        if($this->apid){
            $Apps = M('Apps');
            unset($map);
            $map['apid'] = $this->apid;
            $map['mid'] = $this->mid;
            $this->ap = $Apps->where($map)->find();
            $this->assign('ap', $this->ap);
        }

        $this->assign('addUrl', U($this->con.'/add?apid='.$this->apid));
        $this->assign('editUrl', U($this->con.'/edit?apid='.$this->apid.'&id='.I('id')));
        $this->assign('delUrl', U($this->con.'/delete?apid='.$this->apid.'&id='.I('id')));
        $this->assign('backUrl', U($this->con.'/index'));

        $this->assign('insertUrl', U($this->con.'/insert'));
        $this->assign('updateUrl', U($this->con.'/update'));
        $this->assign('updateField', U($this->con.'/updateField'));
        

        unset($map);
        $User = D('User');
        $map['id'] = $this->mid;
        $this->user = $User->getOne($map);
        $this->assign('user', $this->user);

        layout(true);
        if (array_key_exists('HTTP_X_PJAX', $_SERVER) && $_SERVER['HTTP_X_PJAX'])
        {
            layout(false);
        }

    }

    public function index($curPage =1,$pageSize = 10){
        if (IS_POST) {
            $m = D($this->con);
            if ( method_exists( $this, '_filter' ) ) {
                $map = $this->_filter();
            }
            $map['mid'] = $this->mid;
            $map['apid'] = $this->apid;
            $ret = $m->getPage($map,$curPage,(int)$pageSize);
            $data['success'] = true;
            $data['data'] = $ret['volist'];
            $data['totalRows'] = $ret['count'];
            $data['curPage'] = $curPage;
            $this->ajaxReturn($data);
        }
        $this->display();
    }

    public function edit($id ='')
    {
        $map['mid'] = $this->mid;
        $map['apid'] = $this->apid;
        $map['id'] = $id;
        $model = D($this->con);
        $vo = $model->getOne($map);
        $this->assign('vo', $vo);
        $this->display();
    }

    public function delete($id = 0)
    {
        $map['mid'] = $this->mid;
        $map['apid'] = $this->apid;
        $map['id'] = $id;
        $model = D($this->con);
        $ret = $model->where($map)->delete();
        if ($ret !== FALSE) {
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function insert()
    {
        $model = D( $this->con );
        $_POST['mid'] = $this->mid;
        if ( false === $model->create() ) {
            $this->error( $model->getError() );
        }
        //保存当前数据对象
        $list = $model->add();
        if ( $list !== false ) {
            $this->success( '新增成功!');
        } else {
          //失败提示
          $this->error( '新增失败!' );
        }
    }

    public function add()
    {
        $this->display();
    }

    function update($id = 0)
    {

        $model = D( $this->con );
        if ( false === $model->create() ) {
            $this->error( $model->getError() );
        }
        // 更新数据
        $map['uid'] = $this->uid;
        $map['id'] = $id;
        $list = $model->where( $map )->save();
        if ( false !== $list ) {
          //成功提示
            $this->success( '编辑成功,请手动刷新本页面.', cookie( '_currentUrl_' ) );
        } else {
          //错误提示
          $this->error( '编辑失败!' );
        }
    }

    public function updateField($value='',$pk = '',$name = '')
    {
        if (!$name || !$pk  ) return;
        $model = M( $this->con );
        $map['id'] = $pk;
        $map['mid'] = $this->mid;
        $data[$name] = $value;
        $model->where($map)->save($data);
        $this->success( '成功!');
    }

}
