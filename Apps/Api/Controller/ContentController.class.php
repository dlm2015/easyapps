<?php
namespace Api\Controller;
use Think\Controller;
class ContentController extends CommonController {
    public function index($id = 0){
        $Content = D('Content');
        $map['apid'] = $this->apid;
        $map['id'] = $id;
        $data = $Content->where($map)->find();
        $this->ajaxReturn($data,$this->format);
    }

    public function getList($pageid = 0)
    {
        $Apps = D('Apps');
        $map['apid'] = $this->apid;
        if ($pageid)
        {
            $Page = D('Page');
            $map['id'] = $pageid;
            $data['pageInfo'] = $Page->where($map)->find();
            unset($map['id']);
            $map['pageid'] = $pageid;
        }

        $Content = D('Content');
        $data['data'] = $Content->getPage($map);

        $this->ajaxReturn($data,$this->format);
    }
}
