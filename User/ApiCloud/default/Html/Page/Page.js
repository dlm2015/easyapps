avalon.filters.clearHtml =  function(str) { //用法： {{aaa}}
    str = str.replace(/<\/?[^>]*>/g,''); //去除HTML tag
    str = str.replace(/[ | ]*\n/g,'\n'); //去除行尾空白
    str = str.replace(/\n[\s| | ]*\r/g,'\n'); //去除多余空行
    str=str.replace(/&nbsp;/ig,'');//去掉&nbsp;
    return str;
}

require.config({
    baseUrl: "../../js",
})


require(["EasyApp"], function($) {
    var PageCtl = avalon.define({
        $id: "PageCtl",
        App:{},
        pageInfo:{},
        datalist:[],

        openContentWin:function(el){
            var url = "../Content/index.html?id="+el.id;
            $.openWin(url,'Content');
            return false;
        },
        goHome:function(){
            api.openWin({name:'slidLayout'})
            return false;
        },

        run:function(){
            PageCtl.App = $.App();
            var id = $.getUrlParam('id');
            $.getContentList(PageCtl.App.id,id,function(req){
                PageCtl.datalist = req['data']['volist'];
                PageCtl.pageInfo = req['pageInfo'];
            });
        }
        
    })
    $(function(){
        PageCtl.run();
        avalon.scan();
    })
})
